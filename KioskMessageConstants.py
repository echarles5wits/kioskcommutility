class MessageConstants:
    TOKIOSK_RESET = "RESET"
    TOKIOSK_AVAILABLE = "AVAIL"
    TOKIOSK_UNAVAILABLE = "UNAVAIL"

    @staticmethod
    def get_message_constants():
        print([a for a in dir(MessageConstants) if not a.startswith('__') and not callable(getattr(MessageConstants,a))])
        return [a for a in dir(MessageConstants) if not a.startswith('__') and not callable(getattr(MessageConstants,a))]
