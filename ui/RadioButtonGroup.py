from ui.Button import Button
from utils.ObserverUtils import *

class RadioButtonGroup(Observer):
    def __init__(self, buttons):
        Observer.__init__(self)
        self.buttons = buttons
        self.observe("CLICK", self.eh_handle_radio_button_clicked)

    def eh_handle_radio_button_clicked(self, data):
        if data in self.buttons:
            for b in self.buttons:
                b.deselect()
            data.select()

    def get_selected(self):
        for button in self.buttons:
            if button.is_selected():
                return button

    def contains(self, button):
        return button in self.buttons



