from ui.Button import Button
import pygame as pg

class RadioButton(Button):
    def __init__(self, rect, color, fxn, **kwargs):
        Button.__init__(self,rect,color,fxn, **kwargs)
       #  self.process_kwargs(kwargs)
        self.selected = False
        if 'text' in kwargs:
            self.label = kwargs['text']


    def select(self):
        self.selected = True

    def deselect(self):
        self.selected = False

    def is_selected(self):
        return self.selected

    def get_id(self):
        return self.label

    def update(self, surface):
        super().update(surface)
        if self.selected:
            surface.fill(pg.Color("black"), self.rect)
            surface.fill(self.highlight_color, self.rect.inflate(-4, -4))
            if self.text:
                text_rect = self.text.get_rect(center=self.rect.center)
                surface.blit(self.text, text_rect)
