import asyncio
import atexit
import CommonConfigVars as CommonConfigVars
import pygame
import sys
from math import floor
from ui.Button import Button
from ui.RadioButton import RadioButton
from ui.RadioButtonGroup import RadioButtonGroup
from KioskMessageConstants import MessageConstants
from KioskConnectionManager import KioskConnectionManager
from aiohttp import web


pygame.init()
pygame.font.init()
clock = pygame.time.Clock()
# create fonts here
title_font = pygame.font.SysFont("Arial", 30)
status_font = pygame.font.SysFont("Arial", 20)

# set UI configs here
pygame.mouse.set_visible(True)
guiDisplay = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Kiosk Comm Cheater")

# set up kiosk connections
clients = ["AZTEC_TEMPLE", "AZTEC_GUARDIANS"]
kiosk_connection_manager = KioskConnectionManager(clients)


@atexit.register
def on_system_exit():
    print("Seeya!")


def handle_button_click(button):
    print(button)
    client = radio_group.get_selected()
    if client and not radio_group.contains(button):
        print("Send message " + button.click_event + " to " + client.get_id())
        kiosk_connection_manager.queue_message(client.get_id(), getattr(MessageConstants, button.click_event))



crashed = False
button_array = []
DARK_GREEN = (4, 57, 39)
MID_GREEN = (46, 139, 87)
LIGHT_GREEN = (157, 193, 131)
TAN = (248, 248, 240)
ORANGE = (204, 77, 0)
CHARCOAL = (40, 40, 40)
LIGHT_YELLOW = (255,233,168)
YELLOW = (255, 195, 15)

BUTTON_STYLE = {"text": None,
                "hover_color": MID_GREEN,
                "clicked_color": LIGHT_GREEN,
                "clicked_font_color": ORANGE,
                "hover_font_color": CHARCOAL}

# create buttons that correspond with kiosk messages (to kiosks, not from)
for idx, constant in enumerate(MessageConstants.get_message_constants()):
    BUTTON_STYLE['text'] = constant
    BUTTON_STYLE['click_event'] = constant
    new_button = Button((0, 0, 200, 40), LIGHT_GREEN, handle_button_click, **BUTTON_STYLE)
    new_button.rect.center = (100 + 200 * floor(idx / 11), 50 + 50 * (idx % 11))
    button_array.append(new_button)

# set up client radio buttons

client_buttons = []
for i in range(len(clients)):
    BUTTON_STYLE['text'] = clients[i]
    BUTTON_STYLE['click_event'] = clients[i]
    BUTTON_STYLE['highlight_color'] = YELLOW
    new_button = RadioButton((0, 0, 150, 40), LIGHT_YELLOW, handle_button_click, **BUTTON_STYLE)
    new_button.rect.center = (400 + 500 * floor(i / 11), 50 + 50 * (i % 11))
    client_buttons.append(new_button)
radio_group = RadioButtonGroup(client_buttons)


async def wshandler(request):
    app = request.app
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    # handshake - ask for client name
    await ws.send_str("NAME_PLEASE")
    msg = await ws.receive()
    if msg.type == web.WSMsgType.text:
       # register client with the name given by NAME_PLEASE
        KioskConnectionManager.register_client(msg.data, ws, print_client_message)
        KioskConnectionManager.queue_message(msg.data, "hello, new room!")

    while 1:
        msg = await ws.receive()
        if msg.type == web.WSMsgType.text:
            await KioskConnectionManager.handle_client_message(msg.data)

        elif msg.type == web.WSMsgType.close or \
                msg.type == web.WSMsgType.error:
            break

    print("Closed connection")
    return ws

def print_client_message(msg = None):
    print("Got client message: " + msg)
    return msg

async def handle(request):
    index = open("index.html", 'rb')
    content = index.read()
    return web.Response(body=content, content_type='text/html')

async def enter_game_loop(app):
    while True:
        await asyncio.sleep(0.01)

        await KioskConnectionManager.process_queue()

        events = pygame.event.get()

        for event in events:
            for but in button_array:
                but.check_event(event)
            for but in client_buttons:
                but.check_event(event)

            if event.type == pygame.QUIT:
                sys.exit()  # handles clicking the X in the pygame window

        guiDisplay.fill(TAN)
        for but in button_array:
            but.update(guiDisplay)

        for client_button in client_buttons:
            client_button.update(guiDisplay)

        pygame.display.update()


app = web.Application()
app["sockets"] = []

asyncio.ensure_future(enter_game_loop(app))

app.router.add_route('GET', '/connect', wshandler)
app.router.add_route('GET', '/', handle)


web.run_app(app, host=CommonConfigVars.KIOSK_COMMUNICATION_IP, port=CommonConfigVars.KIOSK_COMMUNICATION_PORT)
