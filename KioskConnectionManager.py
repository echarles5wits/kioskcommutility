import time

class KioskConnectionManager:
    clients = {}
    delimiter = "+"
    message_queue = []

    def __init__(self, client_names):

        self.clients = {}
        self.message_queue = []
        for name in client_names:
            self.clients[name] = 0  # these are all the rooms that are expecting to receive messages

    @staticmethod
    def register_client(client_name, websocket, receive_callback):
        print("Registered " + client_name)
        KioskConnectionManager.clients[client_name] = None
        KioskConnectionManager.clients[client_name] = KioskClient(websocket, receive_callback)

    @staticmethod
    def queue_message(client_id, message):
        KioskConnectionManager.message_queue.append((client_id, message))

    @staticmethod
    async def handle_client_message( msg):
        # figure out which client the message should go to
        parsed_message = msg.split(KioskConnectionManager.delimiter)
        if parsed_message[0] in KioskConnectionManager.clients:
            KioskConnectionManager.clients.get(parsed_message[0]).callback(parsed_message[1])

    @staticmethod
    def on_close(self):
        pass

    @staticmethod
    async def process_queue():
        if len(KioskConnectionManager.message_queue) == 0:
            return
        print("Processing queue at " + str(time.time()))
        client_name, message = KioskConnectionManager.message_queue[0]
        KioskConnectionManager.message_queue = KioskConnectionManager.message_queue[1:]

        await KioskConnectionManager.send_message_to_kiosk(client_name, message)
        print("Done sending message")

    @staticmethod
    async def send_message_to_kiosk(room_name, message):
        if room_name in KioskConnectionManager.clients:
            await send_ws_message(KioskConnectionManager.clients.get(room_name).websocket, message)


class KioskClient:
    websocket = None
    callback = None

    def __init__(self, websocket, callback):
        self.websocket = websocket
        self.callback = callback

async def send_ws_message(ws, message):
    print("Sending message " + message)
    await ws.send_str(message)






